import React, { Component } from 'react';

import Review from './Review';

import { Route, Link } from "react-router-dom";

import db, { categories } from '../api/db.json';

export default class Reviews extends Component {

    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }


    
    handleChange(event) {

        this.props.history.push(`/reviews/${event.target.value}`);
    }

    componentDidUpdate() {
        document.title = 'React-Wines - ' + this.props.match.params.categorie;
    }

    componentWillUnmount() {

        
        document.title = 'React-Wines';
        
    }



    render() {

        return (
            <div>

            <Route path="/reviews/:categorie/:slug" component={Review}/>

            <hr />

            <select name="leselect" id="leselect" onChange={this.handleChange}>
                <option value="all">All</option>
                {
                    categories.map(categorie => {
                        if(this.props.match.params.categorie === categorie.key) {
                            return <option key={categorie.label} value={categorie.key} defaultValue>{categorie.label}</option>
                        } else {
                            return <option key={categorie.label} value={categorie.key}>{categorie.label}</option>
                        }
                    })
                }
            </select>

            <ul>
                {   
            
                    db.reviews.filter(item => item.category === this.props.match.params.categorie || this.props.match.params.categorie === 'all').map((item, key) => {
                        return (
                            <li key={key}>
                            <Link to={`/reviews/${this.props.match.params.categorie}/${item.slug}`}>
                                <h2>{item.title}</h2>
                                <h3>{item.designation}</h3>
                                <h4>{item.points}</h4>
                            </Link>
                            </li>
                        )
                    })
                }
            </ul>

            

                
            </div>
        )
    }
        
}