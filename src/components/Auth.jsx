import React, { Component } from 'react';


export class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            password: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitLogout = this.handleSubmitLogout.bind(this);
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.onLogin(this.state.name);
    }
    handleSubmitLogout(e){
        e.preventDefault();
        this.props.onLogout();
    }

    handleChange(event){
        const name = event.target.name
        this.setState({
            [name] : event.target.value,
        });
    }

    render() {

        return (
            <form onSubmit={this.handleSubmit}> 
                <label>
                    Name:
                    <input type="text" name="name" onChange={this.handleChange} />
                </label>
                <label>
                    Password:
                    <input type="text" name="password" onChange={this.handleChange}/>
                </label>
                <input type="submit" value="submit" />
            </form>
        )
    }
        
}

export class Logout extends Component {

    render() {

        return (
            <form  onSubmit={this.handleSubmitLogout}>
                <input type="submit" value="Logout" />
            </form>
            
        )
    }
        
}