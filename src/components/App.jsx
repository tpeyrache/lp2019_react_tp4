import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";

import Reviews from './Reviews'
import Review from './Review'
import {Login, Logout } from './Auth'
import Home from './Home'
import AddReview from './AddReview'

import logo from '../assets/logo.png'


export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loggedIn: false,
        };
        this.handleLogin = this.handleLogin.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogin(name){
        this.setState({
            loggedIn: true,
            name
        })
    }

    handleLogout(){
        this.setState({
            loggedIn: false,
            name: ''
        })
    }

    render(){
        return <>
        <Router>
            <nav fx="">
                <header>
                    <a href="/">
                        <img src={logo} alt="React-Wines logo" />
                    </a>
                </header>

                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/reviews">Reviews</Link>
                    </li>
                    {
                        this.state.loggedIn ? <li><Link to="/logout">Logout</Link></li> : <li><Link to="/login">Login</Link></li>
                    }
                    <li><Link to="/add-review">Add Review</Link></li>

                </ul>
            </nav>
            <main>
                <section>
                    <Switch>

                        <Route exact path="/add-review">
                            {!this.state.loggedIn ? <Redirect to="/login" /> : <AddReview />}
                        </Route>

                        <Route path="/review/:slug" component={Review} />

                        <Route path="/reviews/:categorie?" component={Reviews} />

                
                        <Route path="/login">
                            {this.state.loggedIn ? <Redirect to="/" /> : <Login onLogin={this.handleLogin}/>}
                        </Route>

                        <Route path="/logout">
                            <Logout onLogout={this.handleLogout} />
                        </Route>

                       

                        <Route path="/">
                            <Home />
                        </Route>
                        

                    </Switch>

                    
                </section>
            </main>

            <footer>
                <p>Université de La Rochelle</p>
            </footer>
        </Router>
        </>
    }
    
}

