import React, { Component } from 'react';

import { categories } from '../api/db.json';


export default class AddReview extends Component {

    constructor(props) {
        super(props);
        this.state = {
            author: '',
            title: '',
            wine: '',
            category: '',
            price: '',
            review: '',

        };
        this.handleChange = this.handleChange.bind(this);
    }


    
    handleChange(event) {
        const name = event.target.name
        this.setState({
            [name] : event.target.value,
            
        });
    }


    render() {
        
        return (
            <form onSubmit={(e) => e.preventDefault()}>
                <label>
                    Author:
                    <input type="text" name="author" onChange={(e) => this.handleChange(e)}/>
                </label>
                <label>
                    Title:
                    <input type="text" name="title"  onChange={(e) => this.handleChange(e)}/>
                </label>
                <label>
                    Wine:
                    <input type="text" name="wine"  onChange={(e) => this.handleChange(e)}/>
                </label>
                <label>
                    Category:
                    <select name="category" onChange={(e) => this.handleChange(e)}>
                    {
                    categories.map(categorie => {
  
                        return <option key={categorie.label} value={categorie.key}>{categorie.label}</option>
                        
                    })
                }
                    </select>
                </label>
                <label>
                    Price:
                    <input type="text" name="price"  onChange={(e) => this.handleChange(e)}/>
                </label>
                <label>
                    Review:
                    <input type="text" name="review"  onChange={(e) => this.handleChange(e)}/>
                </label>
            </form>
        )
    }
        
}