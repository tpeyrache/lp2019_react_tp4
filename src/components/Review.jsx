import React, { Component } from 'react';
import db from '../api/db.json';



export default class Review extends Component {


    render() {
        let data = db.reviews.find(review => review.slug === this.props.match.params.slug)
        console.log(data)
        return (
            <>
            <h1>{data.designation}</h1>
            <h2>{data.title}</h2>
            <h3>{data.points}</h3>
            <p>{data.price} <span>{data.taster_name}</span></p>
            </>
        )
    }
        
}